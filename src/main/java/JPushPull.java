import Util.Enum_Platform;
import cn.jiguang.common.ClientConfig;
import cn.jiguang.common.resp.APIConnectionException;
import cn.jiguang.common.resp.APIRequestException;
import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.*;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.audience.AudienceTarget;
import cn.jpush.api.push.model.notification.AndroidNotification;
import cn.jpush.api.push.model.notification.IosNotification;
import cn.jpush.api.push.model.notification.Notification;
import com.oracle.tools.packager.Log;

import static cn.jpush.api.push.model.notification.PlatformNotification.ALERT;

/**
 * 极光推送 API
 * Created by cjay on 2017/1/13.
 */
public class JPushPull {
    private static String APP_KEY = "";
    private static String MASTER_SECRET = "";
    private static JPushPull instance;
    public PhonePlatformType PType = PhonePlatformType.PLATFORM_ALL;
    Enum_Platform whichPlatform;

    public enum PhonePlatformType {
        PLATFORM_ANDROID, PLATFORM_IOS, PLATFORM_ALL;
    }

    public enum MessageType {
        MSG_SIMPLE, MSG_CUSTOM

    }

    public static JPushPull getInstance(Enum_Platform whichPlatform) {
        if (instance == null) {
            instance = new JPushPull(whichPlatform);
        }
        return instance;
    }

    public JPushPull(Enum_Platform whichPlatform) {
        this.whichPlatform = whichPlatform;
        String[] APIKEY = whichPlatform.getJPushCode().split("\\|\\|\\|");
        APP_KEY = APIKEY[0].toString();
        MASTER_SECRET = APIKEY[1].toString();
    }

    public PushResult sendPush(PushPayload payload) {
        PushResult result = null;
        try {
            JPushClient jpushClient = new JPushClient(MASTER_SECRET, APP_KEY, null, ClientConfig.getInstance());
            result = jpushClient.sendPush(payload);
            Log.info("Got result - " + result);

        } catch (APIConnectionException e) {
            // Connection error, should retry later
            Log.info("Connection error, should retry later" + e);

        } catch (APIRequestException e) {
            // Should review the error, and fix the request
            Log.info("Should review the error, and fix the request" + e);
            Log.info("HTTP Status: " + e.getStatus());
            Log.info("Error Code: " + e.getErrorCode());
            Log.info("Error Message: " + e.getErrorMessage());
        }
        return result;
    }

    /***
     *    所有平台，所有设备，内容为 ALERT 的通知。
     */
    public PushPayload buildPushPayLoad_SimpleMsgPush(String MSG) {
        return PushPayload.alertAll(MSG);
    }


    /***
     *    自定推送 指定 平台 指定 手机 指定讯息
     */
    public PushPayload buildPushPayLoad_CustomPush(PhonePlatformType PType, String[] Tag, String[] alias, String MSG) {
        Platform platform = Platform.all();
        switch (PType) {
            case PLATFORM_ANDROID:
                platform = Platform.android();
                break;
            case PLATFORM_IOS:
                platform = Platform.ios();
                break;
            case PLATFORM_ALL:
                platform = Platform.all();
                break;
        }

        return PushPayload.newBuilder()
                .setPlatform(platform)
                .setAudience(addAudience(Tag, alias))
                .setNotification(addNotification(PType, MSG))
                .setMessage(Message.content(MSG))
                .build();

    }

    private Audience addAudience(String[] Tag, String[] alias) {
        try {
            if (Tag == null && alias == null) {
                return Audience.newBuilder().setAll(true)
                        .build();
            } else if (Tag != null && alias != null) {
                return Audience.newBuilder()
                        .addAudienceTarget(AudienceTarget.tag(Tag))
                        .addAudienceTarget(AudienceTarget.alias(alias))
                        .build();
            } else {
                if (Tag == null) {
                    return Audience.alias(alias);
                } else {
                    return Audience.tag(Tag);
                }
            }
        } catch (Exception ex) {
            return Audience.newBuilder().setAll(true)
                    .build();
        }
    }

    private Notification addNotification(PhonePlatformType PType, String MSG) {
        switch (PType) {
            case PLATFORM_ANDROID:
                return Notification.newBuilder().addPlatformNotification(AndroidNotification.newBuilder()
                        .setAlert(MSG)
                        .addExtra("from", "JPush")
                        .build()).build();
            case PLATFORM_IOS:
                return Notification.newBuilder().addPlatformNotification(IosNotification.newBuilder()
                        .setAlert(MSG)
                        .setBadge(5)
                        .setSound("happy")
                        .addExtra("from", "JPush")//附加字段，让手机可判断从那儿来
                        .build()).build();
            case PLATFORM_ALL:
                return Notification.newBuilder()
                        .addPlatformNotification(IosNotification.newBuilder()
                                .setAlert(MSG)
                                .setBadge(5)
                                .setSound("happy")
                                .addExtra("from", "JPush")
                                .build())
                        .addPlatformNotification(AndroidNotification.newBuilder()
                                .setAlert(MSG)
                                .addExtra("from", "JPush")
                                .build())
                        .build();
        }
        return null;
    }

}
