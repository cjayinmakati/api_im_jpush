package Util;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Util_DES_TripledesECB {
    public static String EncodeKey = "";

    public Util_DES_TripledesECB(String EncodeKey){
        this.EncodeKey = EncodeKey;
    }

    public static String Encrypt(String str) throws Exception{
        byte[] md5Key = GetMd5(EncodeKey); //16bytes
        SecretKey key = new SecretKeySpec(md5Key, "DESede");
        Cipher ecipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
        ecipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] data = str.getBytes("UTF-8");
        byte[] encryptedArray = ecipher.doFinal(data);
        String encryptedString = new Base64().encode(encryptedArray);
        return encryptedString;
    }

    public static String Decrypt(String encryptedString) throws Exception{
        byte[] encryptedArray = new Base64().decode(encryptedString);
        byte[] md5Key = GetMd5(EncodeKey); //16bytes
        SecretKey key = new SecretKeySpec(md5Key, "DESede");
        Cipher dcipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
        dcipher.init(Cipher.DECRYPT_MODE, key);
        byte[] decryptArray = dcipher.doFinal(encryptedArray);
        return new String(decryptArray, "UTF-8");
    }

    public static byte[] GetMd5(String keyString) throws UnsupportedEncodingException, NoSuchAlgorithmException{
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        messageDigest.update(keyString.getBytes("UTF-8"), 0, keyString.length());
        byte[] md5 = messageDigest.digest();
        byte[] rawKey = new byte[24];
        System.arraycopy(md5, 0, rawKey, 0, 16);
        System.arraycopy(md5, 0, rawKey, 16, 8);
        return rawKey;
    }

}
