package Util;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

public class Utils_Http {
	private static final String Method_DEL = "DELETE";
	private static final String Method_PUT = "PUT";
	private static final String Method_POST = "POST";
	private static final String Method_GET = "GET";
	private volatile static Utils_Http instance;
	private static String URL;

	private static RequestType requestType = RequestType.RequestTextType_TX;
//	public static final String RequestTextType_JSON = "JSON";
//	public static final String RequestTextType_XML = "XML";
//	public static final String RequestTextType_TX = "TX";

	public enum RequestType{
		RequestTextType_JSON,RequestTextType_XML,RequestTextType_TX
	}

	public static Utils_Http getInstance(String url, RequestType RequestTextType) {
		if (instance == null) {
			synchronized (Utils_Http.class) {
				if (instance == null) {
					instance = new Utils_Http();
				}
			}
		}
		URL = url;
        requestType = RequestTextType;

		return instance;
	}

	public String Post(HashMap<String, String> Headers, HashMap<String, Object> Parameters, int connectTimeout,
			int readTimeout) {
		return Method_POST(Headers, Parameters, connectTimeout, readTimeout);
	}

	public String Get(HashMap<String, String> Headers, HashMap<String, Object> Parameters, int connectTimeout,
			int readTimeout) {
		return Method_GET(Headers, Parameters);
	}

	public String Put(HashMap<String, String> Headers, HashMap<String, Object> Parameters, int connectTimeout,
			int readTimeout) {
		return Method_PUT(Headers, Parameters, connectTimeout, readTimeout);
	}

	public String Del(HashMap<String, String> Headers, HashMap<String, Object> Parameters, int connectTimeout,
			int readTimeout) {
		return Method_DEL(Headers, Parameters, connectTimeout, readTimeout);
	}

	public String Method_DEL(HashMap<String, String> mRequesHeaders, HashMap<String, Object> Parameters,
			int connectTimeout, int readTimeout) {
		URL _url = null;
		HttpURLConnection connection = null;
		OutputStream out = null;
		BufferedReader br = null;
		StringBuffer sb = new StringBuffer();
		try {
			_url = new URL(URL);
			connection = (HttpURLConnection) _url.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestMethod(Method_DEL);

			for (Entry<String, String> entry : mRequesHeaders.entrySet()) {
				System.out.println("HeaderKey = " + entry.getKey() + ", HeaderContent = " + entry.getValue());
				connection.setRequestProperty(entry.getKey(), entry.getValue());
			}
			connection.connect();

		} catch (Exception ex) {
			return ex.getMessage().toString();
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException ex) {
					throw new RuntimeException(ex);
				} finally {
					if (connection != null) {
						connection.disconnect();
					}
				}
			}
		}
		return sb.toString();
	}

	public String Method_PUT(HashMap<String, String> mRequesHeaders, HashMap<String, Object> Parameters,
			int connectTimeout, int readTimeout) {
		URL _url = null;
		HttpURLConnection connection = null;
		OutputStream out = null;
		BufferedReader br = null;
		StringBuffer sb = new StringBuffer();
		try {
			_url = new URL(URL);
			connection = (HttpURLConnection) _url.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestMethod(Method_PUT);
			for (Entry<String, String> entry : mRequesHeaders.entrySet()) {
				System.out.println("HeaderKey = " + entry.getKey() + ", HeaderContent = " + entry.getValue());
				connection.setRequestProperty(entry.getKey(), entry.getValue());

			}
			out = connection.getOutputStream();
			out.write(formatFormParameters(Parameters).getBytes());
			out.flush();

			br = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
			for (String str = br.readLine(); str != null; str = br.readLine()) {
				sb.append(str);
			}
		} catch (Exception ex) {
			return ex.getMessage().toString();
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException ex) {
					throw new RuntimeException(ex);
				} finally {
					if (connection != null) {
						connection.disconnect();
					}
				}
			}
		}
		return sb.toString();
	}

	public String Method_POST(HashMap<String, String> mRequesHeaders, HashMap<String, Object> Parameters,
			int connectTimeout, int readTimeout) {
		URL _url = null;
		HttpURLConnection connection = null;
		OutputStream out = null;
		BufferedReader br = null;
		StringBuffer sb = new StringBuffer();
		try {
			_url = new URL(URL);
			connection = (HttpURLConnection) _url.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestMethod(Method_POST);
			for (Entry<String, String> entry : mRequesHeaders.entrySet()) {
				System.out.println("HeaderKey = " + entry.getKey() + ", HeaderContent = " + entry.getValue());
				connection.setRequestProperty(entry.getKey(), entry.getValue());

			}
			out = connection.getOutputStream();
			switch (requestType){
				case RequestTextType_JSON:
					out.write(formatToJsonParameters(Parameters).getBytes());
					break;
				case RequestTextType_XML:
					break;
				case RequestTextType_TX:
					out.write(formatFormParameters(Parameters).getBytes());
					break;
			}

			br = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
			for (String str = br.readLine(); str != null; str = br.readLine()) {
				sb.append(str);
			}

		} catch (Exception ex) {
			return ex.getMessage().toString();
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException ex) {
					throw new RuntimeException(ex);
				} finally {
					if (connection != null) {
						connection.disconnect();
					}
				}
			}
		}
		return sb.toString();
	}

	public String Method_GET(HashMap<String, String> mRequesHeaders, HashMap<String, Object> parameters) {
		URL _url = null;
		BufferedReader br = null;
		StringBuffer sb = new StringBuffer();
		HttpURLConnection connection = null;
		try {
			_url = new URL(URL + "?" + formatFormParameters(parameters));
			connection = (HttpURLConnection) _url.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestMethod(Method_GET);
			for (Entry<String, String> entry : mRequesHeaders.entrySet()) {
				System.out.println("HeaderKey = " + entry.getKey() + ", HeaderContent = " + entry.getValue());
				connection.setRequestProperty(entry.getKey(), entry.getValue());

			}
			br = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
			for (String str = br.readLine(); str != null; str = br.readLine()) {
				sb.append(str);
			}
		} catch (Exception ex) {
			return ex.getMessage().toString();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException ex) {
					throw new RuntimeException(ex);
				} finally {
					if (connection != null) {
						connection.disconnect();
					}
				}
			}
		}
		return sb.toString();
	}

	private String formatFormParameters(HashMap<String, Object> map) {
		if (map == null) {
			throw new IllegalArgumentException("args is null");
		}
		StringBuffer sb = new StringBuffer();
		for (Iterator iter = map.entrySet().iterator(); iter.hasNext();) {
			Entry entry = (Entry) iter.next();
			sb.append(entry.getKey()).append("=").append(entry.getValue() != null ? entry.getValue().toString() : "");
			if (iter.hasNext()) {
				sb.append("&");
			}
		}
		return sb.toString();
	}
	private String formatToJsonParameters(HashMap<String, Object> map) {
		if (map == null) {
			throw new IllegalArgumentException("args is null");
		}
		Gson gson = new Gson();
		String Params = gson.toJson(map);
		System.out.println("JSON = "+Params);
		return Params.toString();
	}
}
