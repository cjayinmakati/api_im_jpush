package Util;

import java.security.MessageDigest;

public class Util_SHA {

	public static String encrypt(String s,String TYPE){   
		  MessageDigest sha = null;
		  
		  try{
		   sha = MessageDigest.getInstance(TYPE);   
		   sha.update(s.getBytes());   
		  }catch(Exception e){
		   e.printStackTrace();
		   return "";
		  }

		  return byte2hex(sha.digest());   
		  
		 }

		//↑如果要改 SHA-256 或 MD5 那就直接改紅色的字串，把它給成想要的編碼方法。

		private static String byte2hex(byte[] b){
		     String hs="";
		     String stmp="";
		     for (int n=0;n<b.length;n++){
		      stmp=(Integer.toHexString(b[n] & 0XFF));
		      if (stmp.length()==1) hs=hs+"0"+stmp;
		      else hs=hs+stmp;
		     }
		     return hs.toLowerCase();
		    }
}
