package Util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

/**
 *  @author xiaoxiong
 * @description
 */
public class Util_Md5 {
	/**
	 * md5方式机型加密
	 * 
	 * @param inStr  �?��加密的字符串
	 * @return 返回加密后的数据
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public static String MD5Encrypt(String inStr) throws UnsupportedEncodingException, NoSuchAlgorithmException {
		byte[] source = inStr.getBytes("ascii");
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(source);
		StringBuffer buf = new StringBuffer();
		for (byte b : md.digest())
			buf.append(String.format("%02x", b & 0xff));// %02x
		return buf.toString().toLowerCase(Locale.getDefault());
	}
}
