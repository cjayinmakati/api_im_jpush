package Util;

public enum Enum_Platform {
    PLATFORM_CN("CNINE", "C9娱乐",
            "399ce86f499aba4273fccf84|||a2d49495a22b61c955fc393d",
            "1153161231115890|||zhongcaiappim",
            "YXA6iGijMM8kEeaUqyGQJc9Pjw|||YXA6mqAntTfI8hTeBcFpL-4ENkqFw0Y"
    ),
    PLATFORM_SE("SHUN_EIGHT", "顺8娱乐",
            "d391b8eab95d629f68a6ab0e|||368966baed6b34a7244657ad", "", ""),
    PLATFORM_AB("AOBO", "奥博娱乐",
            "f632d3fc84b3432792e8ac2f|||2129fa7f8c59d6c79ff491c0", "", ""),
    PLATFORM_ZC("ZHONG_CAI", "众彩娱乐",
            "59f6a322072b4f1f6e87957e|||6e0a5af6d913f3a10aca8dce", "", ""),
    PLATFORM_LC("LONG_CAI", "龙彩娱乐",
            "|||", "", ""),
    PLATFORM_LK("LIN_KEN", "林肯娱乐",
            "22bf33cbb96514dafbd94dec|||2e5826d704b112e935e3232d", "", ""),

    PLATFORM_CN_ANALYSIS("CNINE", "C9计画软件",
            "1bec6724888ad0ab6f439fca|||eb57047da29dff9463555e8c",
            "1153161231115890|||zhongcaiappim",
            "YXA6iGijMM8kEeaUqyGQJc9Pjw|||YXA6mqAntTfI8hTeBcFpL-4ENkqFw0Y"
    ),;

    private String code;
    private String name;
    private String JPushCode;
    private String IMclientName;
    private String IMclientCode;

    Enum_Platform(String code, String name, String JPushCode, String IMclientName, String IMclientCode) {
        this.code = code;
        this.name = name;
        this.JPushCode = JPushCode;
        this.IMclientName = IMclientName;
        this.IMclientCode = IMclientCode;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getJPushCode() {
        return JPushCode;
    }

    public String getIMClientName() {
        return IMclientName;
    }

    public String getIMClientCode() {
        return IMclientCode;
    }

    @Override
    public String toString() {
        return this.code;
    }
}
